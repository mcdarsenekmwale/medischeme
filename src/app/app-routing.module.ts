import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import {AnalyzeComponent} from './analyze/analyze.component';

import {DashboardComponent} from './dashboard/dashboard.component';
import {LocationComponent } from './location/location.component';
import {ReportsComponent } from './reports/reports.component';
import {ReportDetailsComponent} from './report-details/report-details.component';
import { GenerateReportComponent } from "./generate-report/generate-report.component";
import { SelectivePreloading } from './selective-preloading';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'analyze', component:  AnalyzeComponent},
  { path: 'map', component: LocationComponent },
  {path: 'report', component: ReportsComponent},
  {path: 'report/:id', component: ReportDetailsComponent},
  {path:'generate-report', component:GenerateReportComponent},
  {path: 'monitor', loadChildren: 'app/monitor-data/monitor-data.module#MonitorDataModule'}
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes,
      {
        enableTracing: true, // <-- debugging purposes only
        preloadingStrategy: SelectivePreloading,

      }
    )
  ],
  exports: [ RouterModule ],
  providers: [
    SelectivePreloading
  ]
})
export class AppRoutingModule { }
