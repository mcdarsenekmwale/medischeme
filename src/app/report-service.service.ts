import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map,retry, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Reports } from './reports';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json','Authorization': 'my-auth-token' })
};

function createHttpOptions(term: string, refresh = false) {

    // e.g., http://npmsearch.com/query?q=dom'
    const params = new HttpParams({ fromObject: { q: term } });
    const headerMap = refresh ? {'x-refresh': 'true'} : {};
    const headers = new HttpHeaders(headerMap) ;
    return { headers, params };
}

@Injectable()
export class ReportServiceService {

//declaring and defining url's
    private reportURL = "http://127.0.0.1:8000/api/";
    private reportsdataUrl = "./assets/json/reports.json";
    private reports:Reports;

  constructor(private http: HttpClient) {
       this.getReports().subscribe(reportlist => {
       });
   }

//retrieve all the reports in the database
   public getReports(): Observable<Reports> {
       return this.http.get<Reports>(this.reportsdataUrl)
       .pipe( retry(3)
       );
   }

//get report details by id
public getReport(id:any){
  const url = `${this.reportsdataUrl}`;
  const new_id = (id-1);
  return this.http.get<Reports>(url)
  .pipe(
      map(data => data[new_id]), // returns a {0|1} element array
      retry(1),
      tap(h => {
        const outcome = h ? `fetched` : `did not find`;
      })
    );
}
  /** POST:the report on the server **/
public saveReports(data:Reports): Observable < Reports > {

    return this.http.post<Reports>(this.reportURL, data, httpOptions).pipe(
        tap((data: any) => this.log(`generated a report=${data.date_created}`)),
        catchError(this.handleError<Reports>('saveReports'))
    );

}
  private log(report: any) {

 }

  /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
  private handleError<T>(operation = 'operation', result ?: T) {
    return (error: any): Observable<T> => {

        // TODO: send the error to remote logging infrastructure
        console.error(error); // log to console instead

        // TODO: better job of transforming error for user consumption
        this.log(`${operation} failed: ${error.message}`);

        // Let the app keep running by returning an empty result.
        return of(result as T);
    };
}
}
