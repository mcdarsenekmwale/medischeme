import { Component, OnInit, ViewChild } from '@angular/core';
import { } from '@types/googlemaps';
// Import your Service and Model

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {

//variables for drawing a chart
  public id = 'chart1';
  public width = 720;
  public height = 460;
  public type = '';
  public dataFormat = 'json';
  public dataSource;

// google maps
  private mapProp;
  private display = false;
  private latitude: number;
  private longitude: number;
  //set your lat and lng 
  private title: string = 'My first AGM project';
  private lat: number = 51.678418;
  private lng: number = 7.809007;
  //private lat: number = -23.586479;
  //private lng: number = -46.682078;

  //Set your google Key here
  constructor(){}


  ngOnInit() {
    //this will be called during initialisation
      this.displayUnimedDistrict();

      
  }




//this function will display and draw districts across where unimed services can be a accessed
  private displayUnimedDistrict(){
    //get data 
    this.type ='maps/malawi';
    
  }

  //draw a map showing were you can access unimed services
  private UnimedClinics(): void {
    //
    this.lat = -13.2512161;
    this.lng = 34.3015278;
    this.display = !this.display;
  }
  //draw a map for zomba
  private drawForZomba():void{
    if (true) { //you have a map for zomba then show having high number of disease cases
      this.lat = -15.3833318;
      this.lng = 35.333332;
      this.display = true;
    }

  }
  //draw a map for Lilongwe
  private drawForLilongwe():void{
    if (true) { //you have a map for zomba then show having high number of disease cases
      this.lat = -13.9833;
      this.lng = 33.7833;
      this.display = true; 
    }
  }
  //draw a map for Blantyre
  private drawForBlantyre():void{
    if (true) { //you have a map for zomba then show having high number of disease cases
      this.lat = -15.499998;
      this.lng = 35;
      this.display = true; 
    }

  }
  //draw a map for Mzuzu
  private drawForMzumzu():void{
    if (true) { //you have a map for zomba then show having high number of disease cases
      this.lat = -11.43896;
      this.lng = 34.00844;
      this.display = true;
    }

  }

  

}
