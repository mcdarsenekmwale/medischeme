import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { catchError, map, retry, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map';
import {Subscribers} from './subscribers';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class SubscribeDataService {

  private subscribeUrl = 'http://localhost:8000/members';

  constructor(private http: HttpClient) {
    this.getSubscriberz().subscribe(data => {
    });
  }

  public getSubscriberz(): Observable<Subscribers> {
    return this.http.get<Subscribers>(this.subscribeUrl);
  }

  public getSubscribers(){
    return Observable.of(this.subscribeUrl);
  }
  public getSubscriber(id: any ) {
    const url = `${this.subscribeUrl}`;
    const new_id = (id-1);
    return this.http.get<Subscribers>(url)
    .pipe(
        map(data => data[new_id]), // returns a {0|1} element array
        retry(1),
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
        })
      );

  }

  /** PUT: update the subscriber json file */
  public updateMember (subscriber :any):Observable<any> {
    return this.http.put(this.subscribeUrl, subscriber, httpOptions).pipe(
      tap(u => {
        const outcome = u ? `updated` : `did not find`;
      })
    );
  }

}
