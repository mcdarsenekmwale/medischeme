import { TestBed, inject } from '@angular/core/testing';

import { MemberServService } from './member-serv.service';

describe('MemberServService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MemberServService]
    });
  });

  it('should be created', inject([MemberServService], (service: MemberServService) => {
    expect(service).toBeTruthy();
  }));
});
