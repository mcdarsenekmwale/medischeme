import { Injectable } from '@angular/core';

import {HttpClient, HttpHeaders} from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Members } from "./members";



@Injectable()
export class AnnualLimitDataService {

  constructor(private http: HttpClient) {
       this.getAnnuallimit().subscribe(data => {
       });
   }

  public getAnnuallimit(): Observable<Members> {
       return this.http.get<Members>("./assets/json/memberAnnualLimit.json")
   }
}
