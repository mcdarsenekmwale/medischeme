import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, ParamMap } from '@angular/router';
import { SubscribeDataService } from '../subscribe-data.service';
import {Subscribers} from '../subscribers';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.css']
})
export class SubscribeComponent implements OnInit {

  constructor(private subscribeDataService : SubscribeDataService,private router: Router, private route: ActivatedRoute) { }

  private subscriberslist:Subscribers;
  private selectedId: number;

//datatables dtOptions
private dtOptions: DataTables.Settings = {};
private dtTrigger: Subject<any> = new Subject();

  ngOnInit(){
    this.getPara();
    this.subscribeDataService.getSubscriberz().subscribe(data => {
      this.subscriberslist =data;
      // Calling the DT trigger to manually render the table
      this.dtTrigger.next();
    });

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      dom:'<"top class"f>tr<"bottom"p><"clear">'

    };

  }

  private getPara():void{
      this.selectedId = +this.route.snapshot.paramMap.get('id');

  }

  private setProgress():void
  {
    // let 30 = this.first[this.firstIndex++];
    // if (this.firstIndex >= this.first.length) this.firstIndex = 0;
    // let last = this.last[this.lastIndex++];
    // if (this.lastIndex >= this.last.length) this.lastIndex = 0;
    // this.userObservable.next({first, last});
  }

  private View(id: number){

    return  this.router.navigate(['./monitor/subscriber/'+id]).then(
      nav=>{console.log(nav);},
      err=>{console.log(err);}
      );
  }
}
