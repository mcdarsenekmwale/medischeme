import 'rxjs/add/operator/switchMap';
import { Component, OnInit, HostBinding, Input, Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { SubscribeDataService } from '../subscribe-data.service';
import { Location } from '@angular/common';
import { Subscribers } from "../subscribers";

@Component({
  selector: 'app-subscriber-details',
  templateUrl: './subscriber-details.component.html',
  styleUrls: ['./subscriber-details.component.css']
})
export class SubscriberDetailsComponent implements OnInit {

    @Input() subscriber$: Observable<Subscribers>;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private location: Location,
              private subscribeDataService : SubscribeDataService) { }

  ngOnInit() {
       this.subscriber$ = this.route.paramMap
       .switchMap((params: ParamMap) =>
         this.subscribeDataService.getSubscriber(params.get('id')));

  }

  private getSubscribe(){
    const id = +this.route.snapshot.paramMap.get('id');
    console.log(id);
    this.subscribeDataService.getSubscriber(id)
      .subscribe(subscriber$ => this.subscriber$ = subscriber$);
  }

  private update(): void {
   this.subscribeDataService.updateMember(this.subscriber$)
     .subscribe(() => this.reload());
 }

 private reload(): void {
  this.location.back();
}

}
