import { Component, OnInit, HostBinding, Input, Pipe, PipeTransform } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MemberServService } from '../member-serv.service';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';
import { Members } from '../members';

@Component({
  selector: 'app-member-detail',
  templateUrl: './member-detail.component.html',
  styleUrls: ['./member-detail.component.css']
})
export class MemberDetailComponent implements OnInit {

  private withRefresh = false;
  @Input() members$: Observable<Members>;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private memberService: MemberServService) { }

  // Push a search term into the observable stream.
  private search(searchTerms$: string) {
    if (searchTerms$) {
      this.memberService.searchMembers(searchTerms$)
        .subscribe(data => this.members$ = data);
    }
  }

  ngOnInit(): void {
    this.members$ = this.route.paramMap
      .switchMap((params: ParamMap) =>
        this.memberService.getMember(params.get('id')));

  }

//a back function which directs the page back to the members page
  private Back():void
  {
    this.location.back();
  }

}
