import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MonitorDataComponent} from './monitor-data/monitor-data.component';
import { MembersComponent } from './members/members.component';
import { MemberDetailComponent} from './member-detail/member-detail.component';
import {SubscribeComponent} from './subscribe/subscribe.component';
import { AnnualLimitComponent } from './annual-limit/annual-limit.component';
import { SubscriberDetailsComponent } from './subscriber-details/subscriber-details.component';

const monitorDataRoutes: Routes = [
  {
    path: '',
    component: MonitorDataComponent,
    children: [
      { path: '', redirectTo: 'members' },
      { path: 'members', component: MembersComponent },
      { path: 'member/:id', component: MemberDetailComponent },
      { path: 'subscribe', component: SubscribeComponent },
      { path: 'subscriber/:id', component: SubscriberDetailsComponent },
      { path: 'annual_limit', component: AnnualLimitComponent }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(monitorDataRoutes)],
  exports: [RouterModule]
})
export class MonitorDataRoutingModule { }
