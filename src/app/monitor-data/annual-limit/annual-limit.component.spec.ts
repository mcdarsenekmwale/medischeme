import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnualLimitComponent } from './annual-limit.component';

describe('AnnualLimitComponent', () => {
  let component: AnnualLimitComponent;
  let fixture: ComponentFixture<AnnualLimitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnualLimitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnualLimitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
