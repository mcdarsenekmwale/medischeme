import { Component, OnInit } from '@angular/core';
import { AnnualLimitDataService } from '../annual-limit-data.service';
import { Subject } from 'rxjs/Subject';
import { Members } from "../members";

@Component({
  selector: 'app-annual-limit',
  templateUrl: './annual-limit.component.html',
  styleUrls: ['./annual-limit.component.css']
})
export class AnnualLimitComponent implements OnInit {

  constructor(private annualDataService : AnnualLimitDataService) { }

  private annuallimit_memberlist:Members;

  //datatables dtOptions
  private dtOptions: DataTables.Settings = {};
  private dtTrigger: Subject<any> = new Subject();

  ngOnInit(){
    this.annualDataService.getAnnuallimit().subscribe(data => {
      this.annuallimit_memberlist =data;
  
      // Calling the DT trigger to manually render the table
      this.dtTrigger.next();
    }); 

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      dom:'<"top class"Bf>tr<"bottom"p><"clear">',
      // Configure the buttons
      // display everything
      order: [[6, "desc"]],
      columnDefs: [{
        "targets": 0,
        "orderable": true
      }],
      deferRender: true,
      scrollCollapse: true
      };
  }

}
