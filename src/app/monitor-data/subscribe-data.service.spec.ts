import { TestBed, inject } from '@angular/core/testing';

import { SubscribeDataService } from './subscribe-data.service';

describe('SubscribeDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubscribeDataService]
    });
  });

  it('should be created', inject([SubscribeDataService], (service: SubscribeDataService) => {
    expect(service).toBeTruthy();
  }));
});
