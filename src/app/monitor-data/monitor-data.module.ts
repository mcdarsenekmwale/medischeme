import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MonitorDataRoutingModule } from './monitor-data-routing.module';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { MonitorDataComponent } from './monitor-data/monitor-data.component';
import { AnnualLimitComponent } from './annual-limit/annual-limit.component';
import { SubscriberDetailsComponent } from './subscriber-details/subscriber-details.component';
import { MembersComponent } from './members/members.component';

import { MemberDetailComponent } from './member-detail/member-detail.component';
import {ProgressBarModule} from "angular-progress-bar";
import { DataTablesModule } from 'angular-datatables';

import { SubscribeDataService } from './subscribe-data.service';
import { AnnualLimitDataService } from './annual-limit-data.service';
import { MemberServService } from './member-serv.service';
//importing the dependenncy that is responsible for piping through the app or searching
import { SearchPipe } from './filter.pipe';


@NgModule({
  imports: [
    ReactiveFormsModule,
    DataTablesModule,
    FormsModule,
    CommonModule,
    ProgressBarModule,
    MonitorDataRoutingModule
  ],
  declarations: [SubscribeComponent, MonitorDataComponent, AnnualLimitComponent, SubscriberDetailsComponent, MembersComponent, SearchPipe, MemberDetailComponent,],
  providers: [SubscribeDataService, AnnualLimitDataService, MemberServService ],
})
export class MonitorDataModule { }
