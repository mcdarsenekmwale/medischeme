import { TestBed, inject } from '@angular/core/testing';

import { AnnualLimitDataService } from './annual-limit-data.service';

describe('AnnualLimitDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AnnualLimitDataService]
    });
  });

  it('should be created', inject([AnnualLimitDataService], (service: AnnualLimitDataService) => {
    expect(service).toBeTruthy();
  }));
});
