import { Component, OnInit, Output, EventEmitter, Input  } from '@angular/core';
import {Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MemberServService } from '../member-serv.service';
import { DataTableDirective } from 'angular-datatables';
import { Observable } from 'rxjs/Observable';
import { Subject }    from 'rxjs/Subject';
import { Location } from '@angular/common';
import * as  $ from "jquery";
import { of }         from 'rxjs/observable/of';
import { SearchPipe } from '../filter.pipe';
import {
   debounceTime, distinctUntilChanged, switchMap
 } from 'rxjs/operators';
import { Members} from '../members';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {

  constructor(
    private memberService : MemberServService,
    private router: Router,
    
    private route: ActivatedRoute ) { }

  private memberlist: Members;

  //for searching purposes
  private error:string;
  // searchings
  private searchTerms:string;
  private search:string;
  @Output() onSearchChange = new EventEmitter<Object>();
  

//datatable options
  private dtOptions: DataTables.Settings = {};
  private dtTrigger: Subject<any> = new Subject();
  private dtColumnDefs: DataTables.Settings ={};

  ngOnInit(){
    this.memberService.getMembers().subscribe(data => {
      this.memberlist  = data;
      // Calling the DT trigger to manually render the table
      this.dtTrigger.next();
      console.log(data)
    }, error => this.error =error.statusText
    );

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 12,
      dom:'<"top class"fl>tr<"bottom"p><"clear">',
      deferRender: true,
      scrollCollapse: true
      // Use this attribute to enable the responsive extension
    };

    
  }

  // Push a search term into the observable stream.
  private filter(searchTerms: string) {
  console.log(searchTerms);
      if (searchTerms) {
        this.memberService.searchMembers(searchTerms)
          .subscribe(data =>
            this.memberlist = data);
      }
    }

    private reload(): void {
    // wait 300ms after each keystroke before considering the term
    debounceTime(300);
    // ignore new term if same as previous term
    distinctUntilChanged();
  }
//searching
  private onChange($event) {
    this.onSearchChange.emit(this.search);
  }
// view individual member
  private View(id: number){

    return this.router.navigate(['./monitor/member/' + id]).then(
      nav=>{console.log(nav);},
      err=>{console.log(err);}
      );
    
  }
}
