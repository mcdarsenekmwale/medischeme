export class Subscribers {
  private membership_id: number;
  private id: number;
  private name: string;
  private cover: string;
  private DOB:string;
  private gender:string;
  private status: string;
  private college: string;
  private cover_total: number ;
  private residence: string;
}
