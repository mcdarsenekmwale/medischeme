import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map,retry, tap } from 'rxjs/operators';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { Members} from './members';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};
function createHttpOptions(term: string, refresh = false) {

    // e.g., http://npmsearch.com/query?q=dom'
    const params = new HttpParams({ fromObject: { q: term } });
    const headerMap = refresh ? {'x-refresh': 'true'} : {};
    const headers = new HttpHeaders(headerMap) ;
    return { headers, params };
}

@Injectable()
export class MemberServService {

  private members: Members;
  private data;
  private membersUrl = 'http://localhost:8000/members';  // URL to web api

  constructor(private http: HttpClient) {
    this.getMembers().subscribe(data => {
    });
  }
//retrieve all the members from the database
  public getMembers(): Observable<Members> {
    //Returns an array of the memebers in database
    return this.http.get<Members>(this.membersUrl)
    .pipe(
      retry(3), // retry a failed request up to 3 times
      catchError(this.handleError)
    );
  }

//get member details by id
  public getMember(id:any){
    const url = `${this.membersUrl}`;
    const new_id = (id-1);
    return this.http.get<Members>(url)
    .pipe(
        map(data => data[new_id]), // returns a {0|1} element array
        retry(1),
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
        })
      );
  }
  /** Search for Members within the list**/
  public searchMembers(term: string): Observable<any> {
    term = term.trim();

    // Add safe, URL encoded search parameter if there is a search term
    const options = term ?
     { params: new HttpParams().set('Name', term) } : {};

    return this.http.get<any>(this.membersUrl, options)
      .pipe(
        catchError(this.handleError)
      );
  }

  private getMemberResponse(): Observable<HttpResponse<any>> {
    return this.http.get<any>(
      this.membersUrl, { observe: 'response' });
    }

    private handleError(error: HttpErrorResponse) {
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      }
      // return an ErrorObservable with a user-facing error message
      return new ErrorObservable(
      'Something bad happened; please try again later.');
    }

    private makeIntentionalError() {
      return this.http.get('not/a/real/url')
      .pipe(
        catchError(this.handleError)
      );
    }
  }
