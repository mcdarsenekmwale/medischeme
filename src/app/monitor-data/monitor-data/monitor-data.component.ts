import { Component, OnInit, HostBinding } from '@angular/core';
import{trigger, state, style, animate, transition} from '@angular/animations';

@Component({
  selector: 'app-monitor-data',
  templateUrl: './monitor-data.component.html',
  styleUrls: ['./monitor-data.component.css'],
  animations:[
    trigger('fryinOut',[state('in', style({height:'*'})),
            transition('*=>void',[
              style({height:'*'}), animate(250, style({height:0}))
            ])])
  ]
})

export class MonitorDataComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }

}
