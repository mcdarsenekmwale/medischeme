import { Pipe, PipeTransform } from '@angular/core';
import { Members} from './members';
@Pipe({
  name : 'searchPipe',
 })
 export class SearchPipe implements PipeTransform {
   public transform(value:any[], key: string, term: string ):any[] {
     return value.filter((item) => {
       if (item.hasOwnProperty(key)) {
         if (term) {
           let regExp = new RegExp('\\b' + term, 'gi');
           return regExp.test(item[key]);
         } else {
           return true;         }
     } else {
        return false;
       }    });
  }
 }



//
// pure: false
// })
// export class SearchPipe implements PipeTransform {
// transform(items: any[], filter: any): any[] {
// if (!items || !filter) {
//   return items;
// }
// // filter items array, items which match and return true will be kept, false will be filtered out
// return items.filter((item: any) => this.applyFilter(item, filter));
// }
//
// /**
// * Perform the filtering.
// *
// * @param {any} any The any to compare to the filter.
// * @param {any} filter The filter to apply.
// * @return {boolean} True if any satisfies filters, false if not.
// */
// applyFilter(any: any, filter: any): boolean {
// for (let field in filter) {
//   if (filter[field]) {
//     if (typeof filter[field] === 'string') {
//
//         return false;
//
//     } else if (typeof filter[field] === 'number') {
//       if (any[field] !== filter[field]) {
//         return false;
//       }
//     }
//   }
// }
// return true;
// }
// }
