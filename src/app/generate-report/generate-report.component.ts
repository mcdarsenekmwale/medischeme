import { Component, OnInit } from '@angular/core';
import { NgForm }   from '@angular/forms';
import { Location } from '@angular/common';
import { ReportServiceService } from "../report-service.service";
import { Reports } from "../reports";
@Component({
  selector: 'app-generate-report',
  templateUrl: './generate-report.component.html',
  styleUrls: ['./generate-report.component.css']
})
export class GenerateReportComponent implements OnInit {

  constructor(private reportService:ReportServiceService,
    private location: Location) { }

  ngOnInit() {
  }

  private goBack() {
    this.location.back();
  }

  private save(data: Reports): void {
    console.log(data);
    if (!data) { return; }
    this.reportService.saveReports(data).subscribe(reports => {
      this.goBack();
     this.location.forward();
    }
    );
  }

}
