import { AfterViewInit, Component, OnInit, Renderer } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ReportServiceService} from '../report-service.service';
import { Subject } from 'rxjs/Subject';
import { Reports } from "../reports";

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

//defining datatables variables
  private dtOptions: DataTables.Settings = {};
  private dtTrigger: Subject<any> = new Subject();

  constructor(
    private reportservice : ReportServiceService,
    private renderer: Renderer,
    private router : Router) { }

    //declare some report variable
  private reportlist:Reports;
  private id:any;
  private data:Reports;
  ngOnInit() {
    this.reportservice.getReports().subscribe(reportlist => {
      this.reportlist = reportlist;
      // Calling the DT trigger to manually render the table
      this.dtTrigger.next();
         this.data = reportlist;
     });

    this.dtOptions = {
      pagingType: 'full_numbers', //number
      pageLength: 10, //items or enteries to be displayed once
      dom: 'iftpr',
      order:[[1, 'asc']], //
      columnDefs: [{ //responsible for setting the table columns to be ordered
        "targets": 0, //colume of index zero
        "orderable": true // order set to true
      }],
//called whe atable row is clicked
      rowCallback: (row: Node, info: any[] | Object, i: number) => {
        // Unbind first in order to avoid any repeation handler
        $('td', row).unbind('click');
        // bind first in order to acess the event handler
        $('td', row).bind('click', (event) => {
          if (event.target.innerText == this.data[i].name) {
             this.router.navigate(["/report/" + [i + 1]]).then(
               nav => {  },
               err => { }
            );
          }
        }

      );
        return row;
      }

    };
  }
}
