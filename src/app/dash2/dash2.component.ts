import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dash2',
  templateUrl: './dash2.component.html',
  styleUrls: ['./dash2.component.css']
})
export class Dash2Component implements OnInit {

  //set fusion chart properties
      private id = 'chart1';
      private width = 1000;
      private height = 380;
      private type = 'area2d';
      private dataFormat = 'jsonurl';
      private dataSource = 'http://localhost:8000/trend7/malaria'; //show malaria  trends

      private trend = 'trend';

      private toggle = false;
      private dataValue = 'http://localhost:8000/trend7/malaria'; //show malaria  trends

      private CreateDiseaseChart(disease_name:string):void{
        switch (disease_name) {
        case 'Malaria' :
          this.dataSource = 'http://localhost:8000/'+this.trend+'/Malaria';
          this.dataValue = '/Malaria';
          this.toggle = !this.toggle;
          break;
        case 'STDs' :
          this.dataSource = 'http://localhost:8000/'+this.trend+'/STD';
          this.dataValue = '/STD';
          this.toggle = !this.toggle;
          break;
        case 'Measles' :
          this.dataSource = 'http://localhost:8000/'+this.trend+'/Influenza';
          this.dataValue = '/Influenza';
          this.toggle = !this.toggle;
          break;
        case 'Meningitis' :
          this.dataSource = 'http://localhost:8000/'+this.trend+'/Allergies';
          this.dataValue = '/Allergies';
          this.toggle = !this.toggle;
          break;
        case 'Dysentery' :
            this.dataSource = 'http://localhost:8000/'+this.trend+'/Typhoid';
            this.dataValue = '/Typhoid';
            this.toggle = !this.toggle;
          break;
        case 'Tuberculosis' :
            this.dataSource = 'http://localhost:8000/'+this.trend+'/Headache';
            this.dataValue = this.dataSource;
            this.toggle = !this.toggle;
          break;
        case '2016' :
            this.trend = 'trend';
            this.dataSource = 'http://localhost:8000/'+this.trend+this.dataValue;
            break;
        case '2017' :
            this.trend = 'trend7';
            this.dataSource = 'http://localhost:8000/'+this.trend+this.dataValue;
            break;
        case '2018' :
            this.trend = 'trend8';
            this.dataSource = 'http://localhost:8000/'+this.trend+this.dataValue;
            break;
          case 'High BloodPressure' :
            this.dataSource = 'http://localhost:8000/'+this.trend+'/Nausea';
            this.toggle = !this.toggle;
            break;
        }
      }
  constructor() { }

  ngOnInit() {
  }

}
