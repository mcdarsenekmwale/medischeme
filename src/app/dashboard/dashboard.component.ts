import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() { }

//declare variables for fusioncharts
  id = 'chart1';
  width = 495;
  height = 350;
  type = 'pie2d';
  dataFormat = 'jsonurl';
  dataSource = 'assets/json/member-distribution.json';

  //declare card titles variables on the dashboard.
  private title = "TRENDS";
  private title_m = " MEMBER DISTRIBUTION";
  private title_c = " CONCERNS";
  private title_this = "";
  private expression = false;

  ngOnInit() {
  }

  // toggle a add concern form
  private toggle():void
  {
    this.expression = true;
  }
  //add a Concern
  private addConcern(data:any):void
  {
    this.expression = false;
  }
}
