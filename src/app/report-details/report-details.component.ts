import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ReportServiceService} from '../report-service.service';
import {Component, OnInit, HostBinding, Input, Pipe} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Location } from '@angular/common';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/switchMap';
import { Reports } from '../reports';

@Component({
  selector: 'app-report-details',
  templateUrl: './report-details.component.html',
  styleUrls: ['./report-details.component.css']
})
export class ReportDetailsComponent implements OnInit {

//declaring of single report variable
  private withRefresh = false;
  @Input() reports$: Observable<Reports>;
  private toggle = false;

  constructor(
    private reportservice: ReportServiceService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    //calling the get report method upon initialisation of the class
    this.getReport();
  }

  private getReport():void{
    this.reports$ = this.route.paramMap
      .switchMap((params: ParamMap) =>
        this.reportservice.getReport(params.get('id')));
  }
//function for viewing all the reports
  private viewAll():void{
    this.router.navigate(['/report']).then(
      succ=>{},
      err=>{}
    ); //go to report page
  }

  private editReport(){
    this.toggle =!this.toggle;
  }
}
