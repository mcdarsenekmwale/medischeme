import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {DataService} from '../data.service';
@Component({
  selector: 'app-analyze',
  templateUrl: './analyze.component.html',
  styleUrls: ['./analyze.component.css']
})
export class AnalyzeComponent implements OnInit {

//defining the attributes of the member distribution on the analayze page
  private id = 'chart1';
  private width = 780;
  private height = 460;
  private type = 'column2d';
  private dataFormat = 'jsonurl';
  private dataSource = 'assets/json/member-distribution.json';
  private data =[];
  private toggle = false;

//an array of months
 private months:any[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

  constructor(private dataService : DataService,
              private location: Location,) { }

private getData():void{}
  //change the toggle format as boolean
  //It displays toggle where you can change type of chart
  get format(){
    return this.toggle ; //return a boolean
  }

//a function that changes the type of chart that you can draw or create
  private setChartType(type:string):void{
    switch(type){
      case 'Bar' : //if a bar chart is preffered to be drawn
        this.type = 'column2d';
        break;
      case 'Pie': //if a pie chart is preffered to be drawn
        this.type = 'pie2d';
        break;
      case 'Line'://if a line chart is preffered to be drawn
        this.type = 'line';
        break;
    }
  }

  //a function that draws or creates the a chart showing patterns or trends of certain diseases
  //it takes the disease as a parameter
  private CreateDiseaseChart(disease_name:string):void{
    switch (disease_name) {
      case 'Malaria': //if a chart of Malaria is preffered to be drawn
        this.dataSource = 'http://localhost:8000/malaria';
      this.toggle = true;
      break;
    case 'STDs' :
      this.dataSource = 'assets/json/std.json';
      this.toggle = true;
      break;
    case 'Measles' :
      this.dataSource = 'assets/json/influenza.json';
      this.toggle = true;
      break;
    case 'Meningitis' :
      this.dataSource = 'assets/json/Nausea.json';
      this.toggle = true;
      break;
    case 'Dysentery' :
        this.dataSource = 'assets/json/malaria.json';
        this.toggle = true;
      break;
    case 'Tuberculosis' :
        this.dataSource = 'assets/json/std.json';
        this.toggle = true;
      break;
    case 'Whooping cough' :
        this.dataSource = 'assets/json/influenza.json';
        this.toggle = true;
        break;
    case 'High BloodPressure': //if a chart of High blood pressure is preffered to be drawn
        this.dataSource = 'assets/json/Nausea.json';
        this.toggle = true;
        break;
    }

  }

//create a graph of location, disease during a certain month
private create(data){
  console.log(data);
  if (!data) { return; }
  this.dataService.createLocationGraph(data).subscribe(data => {

  });
   this.dataService.getLocationGraph().subscribe(data=>
    {

    }
  );
   return this.location.forward();
}

  ngOnInit() {
    this.getData();
  }

  //print function
  private Print():void{

  }

}
