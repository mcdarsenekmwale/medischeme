import { Injectable } from '@angular/core';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import { catchError, map, retry, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class DataService {

  private data:any;
  private dataUrl:string = "";
  constructor( private http: HttpClient) {

  }

  public getData(): void{
    this.http.get<any>('assets/json/malaria.json').subscribe(data => {
    this.data = data;
  
  });
  }

  public createLocationGraph(data: any): Observable < any > {
      return this.http.post<any>(this.dataUrl, data, httpOptions).pipe(
          retry(1)
      );
  }

public getLocationGraph(): Observable < any > {
    return this.http.get<any>(this.dataUrl).pipe(
        retry(1)
    );
  }
}
