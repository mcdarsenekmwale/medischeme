export class Reports {
  //report attributes
  private id:number; //for indexing
  private title:string;
  private category:string;
  private date_created:string;
  private description:string;
  private cost:string;
  private generated_by:string;
}
