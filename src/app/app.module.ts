/*In this app module typescript file we import all the dependencies/modules that will be used by the system when executing*/
/*Importing all the angular modules*/
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// Import this library
import { MapsModule, GoogleMapsConfig } from 'angular-googlemaps';
import { LAZY_MAPS_API_CONFIG } from '@agm/core';

//importing all the services that the app will need to call data
import { DataService } from './data.service';
import { ReportServiceService } from './report-service.service';

//importing the modules that is responsible for graphs or charts and maps drawing
import * as FusionCharts from 'fusioncharts';
import * as Charts from 'fusioncharts/fusioncharts.charts';
import * as FintTheme from 'fusioncharts/themes/fusioncharts.theme.fint';
import * as Maps from 'fusioncharts/fusioncharts.maps';
import * as USA from 'fusioncharts/maps/fusioncharts.malawi';
import { FusionChartsModule } from 'angular4-fusioncharts';

//importing the reponsible for the progress bar
import {ProgressBarModule} from "angular-progress-bar";

//importing the modules that is responsible for map drawing using coordinates of different location
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletDrawModule } from '@asymmetrik/ngx-leaflet-draw';
import { AgmCoreModule } from '@agm/core';

//importing the modules that is responsible presenting data in the tables
import { DataTablesModule } from 'angular-datatables';


//importing all the Components that are being declared in the app
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppRoutingModule } from './app-routing.module';
import { Dash2Component } from './dash2/dash2.component';
import { AnalyzeComponent } from './analyze/analyze.component';
import { LocationComponent } from './location/location.component';
import { ReportsComponent } from './reports/reports.component';
import { GenerateReportComponent } from './generate-report/generate-report.component';
import { ReportDetailsComponent } from './report-details/report-details.component';


FusionChartsModule.fcRoot(FusionCharts, Charts, FintTheme,Maps,USA);


@NgModule({
  //components declararion
  declarations: [
    AppComponent,
    DashboardComponent,
    Dash2Component,
    AnalyzeComponent,
    LocationComponent,
    ReportsComponent,
    GenerateReportComponent,
    ReportDetailsComponent
  ],
  //modules being imported and defined in the system
  imports: [
    FormsModule,
    BrowserModule,
    FusionChartsModule,
    DataTablesModule,
    AppRoutingModule,
    HttpClientModule,
    ProgressBarModule,
   // MapsModule,
    AgmCoreModule.forRoot({
      apiKey: 'YOUR_KEY'
    }),
    LeafletModule.forRoot(),
    LeafletDrawModule.forRoot()
  ],
  //services called and used in the app
  //configure your provider like this to send your key
  providers: [DataService, ReportServiceService, { provide: LAZY_MAPS_API_CONFIG, useClass: GoogleMapsConfig }],
  bootstrap: [AppComponent]
})
export class AppModule { }
